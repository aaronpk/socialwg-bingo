(function(){

	var data = {
		"events": [
					"Do you have a URL for that?"
				   ,"Issue-19"
				   ,"Schema.org++"
				   ,"Schema.org--"
				   ,"\"How will you test that?\""
				   ,"\"Is this implemented yet?\""
				   ,"Linked Data++"
				   ,"Linked Data--"
				   ,"Someone says something is easy but hasn't done it."
				   ,"\"Need more evidence\""
				   ,"Semantic Web++"
				   ,"Semantic Web--"
				   ,"\"Ontologies!\""
				   ,"\"Out of scope\""
				   ,"\"We need JSON-LD\""
				   ,"\"We don't need JSON-LD\""
				   ,"floop"
				   ,"\"Can we please change the topic now?\""
				   ,"\"We need to support legacy <something>\""
				   ,"Email++"
				   ,"Email--"
				   ,"\"How do I create an issue/action?\""
				   ,"Hardware problem"
				   ,"Hilarious IRC backchannel comments"
				   ,"httpRange-14"
				   ,"Innappropriate Loqi interjection"
				   ,"URN vs URL"
				   ,"Deliberately controversial statement"
				   ,"Background baby noises"
				   ,"Appeal to timbl"
				   ,"FOAF"
				   ,"Excellent chairing (ratholing successfully shut down)"
		],
		"game": {
			"name": "",
			"events": [],
			"date": ""
		}
	};

	function init(){
		setup();
		var save_name = document.getElementById("save");
		save.addEventListener('click', function(event){
			event.preventDefault();
			storeName();
		})
		var reload = document.getElementById("reload");
		reload.addEventListener('click', function(){
			setup();
			updateScore();
		})
	}

	function setup() {
		data.game.date = new Date();
		data.game.events = [];
		console.log(data.game);
		var events = data.events;
		events.sort(function() {return 0.5 - Math.random()});
		var set = events.slice(0,9);
		var squares = document.getElementById("grid").getElementsByClassName("w1of3");
		for(var i=0; i < squares.length; i++){
			squares[i].getElementsByTagName("p")[0].removeClass('color1-bg');
			squares[i].getElementsByTagName("p")[0].innerHTML = set[i];
			squares[i].removeEventListener('click', score);
			squares[i].addEventListener('click', score);
		}
	}

	var score = function(event){
		ele = event.target;
		ele.className = ele.className + " color1-bg";
		this.removeEventListener('click', score);
		data.game.events.push(ele.innerHTML);
		updateScore();
		console.log(data.game);
	}

	function updateScore(){
		if(data.game.events.length == 9){
			document.getElementById("score").innerHTML = "BINGO!";
		}else{
			document.getElementById("score").innerHTML = data.game.events.length;
		}
	}

	function storeName(){
		var name_input = document.getElementById("name");
		name_input.addEventListener('keypress', function(e){
			name_input.removeClass("fail");
		});
		var name = name_input.value;
		name = name.trim();
		if(name == ""){
			name_input.className = name_input.className + " fail";
		}else{
			data.game.name = name;
			console.log(data.game);
			document.getElementById("namebar").innerHTML = "<span>" + name + "</span>";
		}
	}

	document.addEventListener('DOMContentLoaded', init);

})();

HTMLElement.prototype.removeClass = function(remove) {
    var newClassName = "";
    var i;
    var classes = this.className.split(" ");
    for(i = 0; i < classes.length; i++) {
        if(classes[i] !== remove) {
            newClassName += classes[i] + " ";
        }
    }
    this.className = newClassName;
}